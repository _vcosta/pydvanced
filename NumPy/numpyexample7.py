import numpy as np

x = np.array([[1,2],[3,4]])
y = np.array([[5,6],[7,8]])
v = np.array([9,10])
w = np.array([11, 12])
# Produto interno/vetorial =>  219
print(v.dot(w))
print(np.dot(v, w))
# => [29 67]
print(x.dot(v))
print(np.dot(x, v))
# [[19 22]
#  [43 50]]
print(x.dot(y))
print(np.dot(x, y))
