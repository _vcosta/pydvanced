import numpy as np

x = np.array([1, 2])   		# datatype
print(x.dtype)           	# Saída: "int64"

x = np.array([1.0, 2.0])   	# Seleção automática do datatype
print(x.dtype)              # Saída: "float64"

x = np.array([1, 2], dtype=np.int64)    # Seleção explícita de um datatype
print(x.dtype)              # Saída: "int64"
