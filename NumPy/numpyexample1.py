import numpy as np
a = np.array([1, 2, 3])   	# Cria um array unidimensional
print(type(a))                # Saída: "<class 'numpy.ndarray'>"
print(a.shape)              # Saída: "(3,)"
print(a[0], a[1], a[2])     # Saída: "1 2 3"
a[0] = 5                        # Altera um elemento do array
print(a)                        # Saída: "[5, 2, 3]"
b = np.array([[1,2,3],[4,5,6]])    # Cria um array bidimensional
print(b.shape)                          # Saída: "(2, 3)"
print(b[0, 0], b[0, 1], b[1, 0])     # Saída: "1 2 4"
