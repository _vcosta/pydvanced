import numpy as np
# Cria uma matriz 3x4
# [[ 1  2  3  4]
#  [ 5  6  7  8]
#  [ 9 10 11 12]]
a = np.array([[1,2,3,4], [5,6,7,8], [9,10,11,12]])
# Usa “slicing” para criar uma “view” da matriz original, extraindo as linhas 0 e 1, colunas 
# 1 e 2:
# [[2 3]
#  [6 7]]
b = a[:2, 1:3]
# Ilustrando o conceito de “view” (aponta para os mesmos dados da matriz original)
print(a[0, 1])   # Saída: "2"
b[0, 0] = 77     # b[0, 0] <-> a[0, 1]
print(a[0, 1])   # Saída: "77"
