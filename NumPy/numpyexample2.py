import numpy as np
a = np.zeros((2,2))   # Cria um array com os elementos todos nulos
print(a)              # Saída: "[[ 0.  0.][ 0.  0.]]"     
b = np.ones((1,2))    # Cria um array com os elementos todos unitários
print(b)              # Saída: "[[ 1.  1.]]"
c = np.full((2,2), 7) # Cria um array com todos os elementos iguais a 7
print(c)              # Saída: "[[ 7.  7.][ 7.  7.]]"
d = np.eye(2)         # Cria uma matriz identidade 2x2
print(d)              # Saída: "[[ 1.  0.][ 0.  1.]]"
e = np.random.random((2,2))  # Cria uma matriz 2x2 com valores aleatórios
print(e)              # Uma saída possível: "[[ 0.91940167  0.08143941]
                      #                      [ 0.68744134  0.87236687]]"
