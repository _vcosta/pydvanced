from random import *
numeros = list(range(1, 101))  # compatível com python 3
print(numeros)
# Versão A
def dobro(x):
    return x*2
dobrados1 = list(map(dobro, numeros))
print(dobrados1)
# Versão B (lambda)
dobrados2 = list(map(lambda x: x * 2, numeros))
print(dobrados2)
