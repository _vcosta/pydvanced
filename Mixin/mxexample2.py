class Story(object):
    def __init__(self, name, content):
        self.name = name
        self.content = content
class StoryHTMLMixin(object):
    def render(self):
        return (
            "<html><title>%s</title><body>%s</body></html>" %
            (self.name, self.content))
class StoryHTML(Story, StoryHTMLMixin):
    pass
print (StoryHTML(u'TÍTULO', u'CONTEÚDO').render())
