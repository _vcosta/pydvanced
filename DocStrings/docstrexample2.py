def square(a):
    '''Returns argument a is squared.'''
    return a**a
help(square)
