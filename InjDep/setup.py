# -*- coding: utf-8 -*-
from setuptools import setup
setup(
    name='InjDep',
    packages=['src', 'test'],
    test_suite='test',
)
