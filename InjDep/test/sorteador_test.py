from unittest import TestCase
import src.sorteador as di
class SorteadorTestes(TestCase):
	def teste_sortear(self):
		sorteador = di.Sorteador(10)
		di.random = lambda: 0.5
		sorteio = sorteador.sortear()
		self.assertEqual(5, sorteio)
		di.random = lambda: 0.54
		sorteio = sorteador.sortear()
		self.assertEqual(5, sorteio)
		di.random = lambda: 0.56
		sorteio = sorteador.sortear()
		self.assertEqual(6, sorteio)
