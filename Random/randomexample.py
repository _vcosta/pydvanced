from random import *

seed() #inicia a semente dos número pseudo randômicos
print(randrange(0, 9, 2)) # pares entre 0 e 9
print(choice('abcdefghij')) # seleciona um dos elementos aleatoriamente
items = [1, 2, 3, 4, 5, 6, 7]
shuffle(items) # embaralha os itens aleatoriamente
print(items)
print(randrange(0, 9)) # faixa de inteiro
print(uniform(0, 9)) # faixa de ponto flutuante
print(random()) # retorna no intervalo [0..1]

# -----------------------------

names = ['Tom', 'Harry', 'Andrew', 'Robert']

print(choice(names))         
print(choice(names))         
shuffle(names)
print(names)                        
print(sample(names, 2))
print(sample(names, 2))
print(names)

