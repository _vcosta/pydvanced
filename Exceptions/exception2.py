import sys

randomList = ['a', 0, 2]

for entry in randomList:
    try:
       print("The entry is", entry)
       r = 1/int(entry)
       break
    except ValueError:
       print("ValueError")
       print("Next entry.")
       print()
       pass
    except (TypeError, ZeroDivisionError):
       print("ZeroDivisionError")
       print("Next entry.")
       print()
       pass
    except:
       print("Oops!",sys.exc_info()[0],"occured.")
       print("Next entry.")
       print()
       pass
print("The reciprocal of",entry,"is",r)
