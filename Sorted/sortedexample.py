
x = [2, 8, 1, 4, 6, 3, 7] 
  
print ("Sorted List returned :", sorted(x)) 
  
print ("\nReverse sort :", sorted(x, reverse = True)) 
  
print ("\nOriginal list not modified :", x)

# List 
lista = ['q', 'w', 'r', 'e', 't', 'y'] 
print (sorted(lista)) 
  
# Tuple 
tupla = ('q', 'w', 'e', 'r', 't', 'y') 
print (sorted(tupla)) 
  
# String-sorted based on ASCII translations 
strAux = "python"
print (sorted(strAux))

# Dictionary 
dicAux = {'q':1, 'w':2, 'e':3, 'r':4, 't':5, 'y':6} 
print (sorted(dicAux)) 
  
# Set 
setAux = {'q', 'w', 'e', 'r', 't', 'y'} 
print (sorted(setAux)) 
  
# Frozen Set 
setFAux = frozenset(('q', 'w', 'e', 'r', 't', 'y')) 
print (sorted(setFAux))

listAux = ["cccc", "b", "dd", "aaa"] 
print ("Normal sort :", sorted(listAux)) 
print ("Sort with len :", sorted(listAux, key = len)) 

# Sort a list of integers based on 
# their remainder on dividing from 7 
  
def func(x): 
    return x % 7
  
listAuxNum = [15, 3, 11, 7] 
  
print ("Normal sort :", sorted(listAuxNum)) 
print ("Sorted with key:", sorted(listAuxNum, key = func))

# take second element for sort
def takeSecond(elem):
    return elem[1]

# random list
random = [(2, 2), (3, 4), (4, 1), (1, 3)]

# sort list with key
sortedList = sorted(random, key=takeSecond)

# print list
print('Sorted list:', sortedList)

