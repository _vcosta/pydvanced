FROM python:3-alpine
LABEL maintainer="Vinicius Costa - vinicius.costa@semantix.com.br"
LABEL Version="1.0"

RUN set -e && \
    pip install pytest \
                pytest-xdist

WORKDIR /code