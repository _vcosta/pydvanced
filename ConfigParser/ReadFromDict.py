import configparser

cfg_data = {
    'mysql': {'host': 'localhost', 'user': 'user7',
              'passwd': 's$cret', 'db': 1, 'lista': ['1',2]}
}

config = configparser.ConfigParser()
config.read_dict(cfg_data)

host = config['mysql']['host']
user = config['mysql']['user']
passwd = config['mysql']['passwd']
db = config['mysql']['db']

print(host, cfg_data['mysql']['host'])
print(user)
print(passwd)
print(type(db), type(config['mysql']['lista']) )

print(dir(config))