![semantix](https://www.semantix.com.br/images/logo-semantix-negativo.png)

# Big Data Hiring Camp

This repo contains code and artifacts to build and create docker images used in Big Data Hiring Camp.

Hadoop, Hive and Spark were composed from [docker-hadoop-spark-workbench](https://github.com/big-data-europe/docker-hadoop-spark-workbench)


## Docker Composed Images

Our docker-compose uses the following images:
    
- Hadoop/HDFS
    - Namenode: bde2020/hadoop-namenode:1.1.0-hadoop2.8-java8
    - Datanode: bde2020/hadoop-datanode:1.1.0-hadoop2.8-java8
    
- Hive
    - Hive Server: bde2020/hive:2.1.0-postgresql-metastore
    - Hive Metastore: bde2020/hive:2.1.0-postgresql-metastore
    - Hive Client: semantix/hive-client (docker/Dockerfile_hive_client - Used to create databases and tables during Docker-compose up)

- Spark
    - Spark Master: bde2020/spark-master:2.1.0-hadoop2.8-hive-java8
    - Spark Workder: bde2020/spark-worker:2.1.0-hadoop2.8-hive-java8
    - Spark Notebook:  bde2020/spark-notebook:2.1.0-hadoop2.8-hive
    
- Hue
    - Hue: bde2020/hdfs-filebrowser:3.11
    
- Edgenode:
    - Datanode: semantix/datanode (docker/Dockerfile_datanode)

- Evaluator:
    - Flask/Notebooks: semantix/evaluator (docker/Dockerfile)

## About the test:

- Each applicant has your own (pseudo) cluster, flask app and notebooks
- Each notebook has a question and applicants must implement functions to solve specific problems proposed on questions
- After implementation and tests, applicant can submit the code to Evaluator API (Flask App)
- The flask app will correct the question and then write results to remote database
- Applicants can submit solutions with their e-mail and password n times they want and receive results after submission

## About the Evaluator:

- The evaluator assess solutions submitted by applicants comparing dataframe inserted on applicant db with equivalent dataframe from answers db on Hive (entrypoint: '/question/correct/<question:id>')
- Flask App authenticates (entrypoint '/login') applicants based on data registered (entrypoint '/register') in remote database (in this case: MySql)


## Build and Run

- Extract File

```bash
tar -xvzf smtx-bd-hire-camp-test.tar.gz
mv smtx-bd-hire-camp-test smtx-bd-hire-camp
cd smtx-bd-hire-camp
cat README.md
```

- Build Application (Flask and Notebooks) and edgenodes images

```bash
sudo su
docker build -f docker/Dockerfile -t semantix/evaluator .
docker build -f docker/Dockerfile_datanode -t semantix/datanode .
docker build -f docker/Dockerfile_hive_client -t semantix/hive-client .

```

- Start application with docker-compose wrapped by shell script `./hadoop/docker/start-hadoop-spark-workbench-with-Hive.sh`

```bash
cd hadoop/docker/
sh start-hadoop-spark-workbench-with-Hive.sh
```

 - Create user for test
 
```bash
curl -X POST http://0.0.0.0:5001/register -d "email=nome@semantix.com.br&password=senha&name='Nome Completo'"
exit
```

- Getting Notebook tokens

```bash
# First, get CONTAINER ID:
docker ps # look for container id from evaluator container
# Get logs to CONTAINER_ID
docker logs CONTAINER_ID_semantix/evaluator

# Get Token as following example:

[I 19:00:12.116 NotebookApp] Writing notebook server cookie secret to /run/user/1000/jupyter/notebook_cookie_secret
[I 19:00:15.431 NotebookApp] Serving notebooks from local directory: /home/semantix/bigdata_hiring/smtx-bd-hire-camp
[I 19:00:15.431 NotebookApp] The Jupyter Notebook is running at:
[I 19:00:15.431 NotebookApp] http://localhost:8888/?token=46a6d49e5d72a1324e1bdefecd69ab073994a007501f6729
[I 19:00:15.432 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 19:00:15.436 NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file:///run/user/1000/jupyter/nbserver-14376-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/?token=46a6d49e5d72a1324e1bdefecd69ab073994a007501f6729

```

- Log on Jupyter with token, open some of the question notebooks and test connections (Hive Metastore and Flask with database) by running some cell which start spark application and submit some answer

```python
from pyspark.sql import SparkSession

spark = SparkSession\
    .builder\
    .appName("Exercise Submission")\
    .config("hive.metastore.uris", "thrift://hive-metastore:9083")\
    .enableHiveSupport()\
    .getOrCreate()

email = ""
password = ""

%run 'scripts/correcao.py'
correct_question(email, password, 1)
```
 
- Debug inside container
    - To debug inside some running container it's possible to run a bash in the container, as the following example:
    
    ```bash
    # Get CONTAINER ID
    docker ps
    
    CONTAINER ID        IMAGE                                             COMMAND                  CREATED             STATUS                    PORTS                                                      NAMES
    ac70d82623b2        semantix/evaluator                                "sh /evaluator/start…"   15 minutes ago      Up 15 minutes             0.0.0.0:5001->5001/tcp, 0.0.0.0:8888->8888/tcp             evaluator
    0ed5376328e5        bde2020/spark-worker:2.1.0-hadoop2.8-hive-java8   "entrypoint.sh /bin/…"   15 minutes ago      Up 15 minutes (healthy)   0.0.0.0:8081->8081/tcp                                     docker_spark-worker_1_75e6a3b0fd99
    4e657d1e56a0        bde2020/spark-notebook:2.1.0-hadoop2.8-hive       "/entrypoint.sh /run…"   15 minutes ago      Up 15 minutes             0.0.0.0:9001->9001/tcp                                     spark-notebook
    28a7f54ed009        bde2020/hdfs-filebrowser:3.11                     "/entrypoint.sh buil…"   15 minutes ago      Up 15 minutes             0.0.0.0:8088->8088/tcp                                     docker_hue_1_9ce4192c5bc3
    2565f529bd68        bde2020/spark-master:2.1.0-hadoop2.8-hive-java8   "entrypoint.sh /bin/…"   15 minutes ago      Up 15 minutes (healthy)   0.0.0.0:7077->7077/tcp, 6066/tcp, 0.0.0.0:8080->8080/tcp   spark-master
    a9e01d632e38        bde2020/hive:2.1.0-postgresql-metastore           "entrypoint.sh /bin/…"   17 minutes ago      Up 17 minutes             0.0.0.0:10000->10000/tcp, 0.0.0.0:10002->10002/tcp         hive-server
    33b63fc98fc4        bde2020/hive:2.1.0-postgresql-metastore           "entrypoint.sh /opt/…"   17 minutes ago      Up 17 minutes             10000/tcp, 0.0.0.0:9083->9083/tcp, 10002/tcp               hive-metastore
    1eaf1e454638        bde2020/hadoop-datanode:1.1.0-hadoop2.8-java8     "/entrypoint.sh /run…"   18 minutes ago      Up 17 minutes (healthy)   0.0.0.0:50075->50075/tcp                                   datanode
    baeaafa6b9d2        bde2020/hadoop-namenode:1.1.0-hadoop2.8-java8     "/entrypoint.sh /run…"   18 minutes ago      Up 18 minutes (healthy)   0.0.0.0:8020->8020/tcp, 0.0.0.0:50070->50070/tcp           namenode
    01fa6dbe9b6d        bde2020/hive-metastore-postgresql:2.1.0           "/docker-entrypoint.…"   18 minutes ago      Up 18 minutes             0.0.0.0:5432->5432/tcp                                     docker_hive-metastore-postgresql_1_38a4fee52454

    docker exec -i -t CONTAINER_ID /bin/bash
    ```

- Access log and error log from gunicorn
    - When running a bash on evaluator container, we can get log files path from process
    ```bash
    ps -ef | grep gunicorn

    root         6     1  0 22:32 ?        00:00:00 /usr/bin/python3.6 /usr/bin/gunicorn -w 1 -b 0.0.0.0:5001 app:app --access-logfile /root/guinicorn_access.log --error-logfile /root/guinicorn_error.log

    tail -f  /root/guinicorn_access.log /root/guinicorn_error.log
    
    ==> /root/guinicorn_error.log <==
    [2019-04-10 22:32:17 +0000] [6] [INFO] Starting gunicorn 19.8.1
    [2019-04-10 22:32:17 +0000] [6] [INFO] Listening at: http://0.0.0.0:5001 (6)
    [2019-04-10 22:32:17 +0000] [6] [INFO] Using worker: sync
    [2019-04-10 22:32:17 +0000] [11] [INFO] Booting worker with pid: 11

    ==> /root/guinicorn_access.log <==
    179.228.81.124 - - [10/Apr/2019:22:45:13 +0000] "POST /login?email=amom.mendes@semantix.com.br&name=Amom%20Mendes&password=root HTTP/1.1" 200 600 "-" "PostmanRuntime/7.6.0"
    ```
 
- Kill all process
 ```bash
 docker kill `echo $(docker ps -a -q)`
  ```
 
- Clean all
 ```bash
 docker system prune --all
  ```
 
### ToDos

- Update python version on Spark master and worker 