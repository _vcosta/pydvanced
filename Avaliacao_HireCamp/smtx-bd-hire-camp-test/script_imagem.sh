scp -r -i ODHSemantix.pem ~/Desktop/Imagem/  hadoop@34.207.203.67:~/ 

ssh -i OneDayHiringKey centos@54.159.142.167

sudo yum install https://centos7.iuscommunity.org/ius-release.rpm
sudo yum erase git
sudo yum install epel-release 
sudo yum install git2u

ssh -i OneDayHiringKey hadoop@54.159.142.167

mkdir Projects
cd Projects
git clone https://github.com/SemantixInfinitePossibilities/evaluator.git

cd evaluator
python3.6 -m venv venv
source venv/bin/activate

pip install -r requirements.txt

cd ../

mkdir notebooks
cd notebooks
python3.6 -m venv venv
source venv/bin/activate
pip install pyspark
pip install pandas
pip install jupyter

jupyter notebook --generate-config \
c.NotebookApp.ip = '0.0.0.0' \
c.NotebookApp.allow_origin = '*'

mv ~/Imagem/one-day-hiring ./

hive -f ~/Imagem/databases.sql

hdfs dfs -mv /user/hive/warehouse /user/hive/warehouse_2
hdfs dfs -copyFromLocal ~/Imagem/warehouse /user/hive/

hdfs dfs -mkdir -p /user/hadoop
hdfs dfs -copyFromLocal ~/Imagem/resources /user/hadoop

mv ~/Imagem/*.sh ~/
