from db import db
import importlib
import inflection
from models.result import ResultModel
from models.event_x_question import EventXQuestionModel
from models.question import QuestionModel
from datetime import datetime
from sqlalchemy import select, and_


class QuestionServices:

    @staticmethod
    def get_class(question_class):
        module = importlib.import_module(f'questions.{inflection.underscore(question_class)}')
        question = getattr(module, question_class)
        return question()

    @staticmethod
    def upsert_result(result):
        old_result = ResultModel.query \
            .filter(ResultModel.id_event == result.id_event) \
            .filter(ResultModel.id_question == result.id_question) \
            .filter(ResultModel.id_user == result.id_user) \
            .first()

        if not old_result:
            db.session.add(result)
        else:
            old_result.points = result.points
            old_result.submit_date = datetime.utcnow()

        db.session.commit()

    @staticmethod
    def correct_question(id_question, id_event, id_applicant):
        question_model = QuestionModel.query\
            .filter(QuestionModel.id_question == id_question)\
            .first()

        question = QuestionServices.get_class(question_model.question_class)
        points_percent, message = question.correct()

        event_x_question = EventXQuestionModel.query\
            .filter(EventXQuestionModel.id_event == id_event) \
            .filter(EventXQuestionModel.id_question == id_question)\
            .first()

        total_points = event_x_question.points * points_percent

        result = ResultModel(id_event=id_event, id_user=id_applicant, id_question=id_question, points=total_points)
        QuestionServices.upsert_result(result)

        return total_points, event_x_question.points, message

    @staticmethod
    def get_questions(id_event, id_applicant):

        result_set = db.session.query(EventXQuestionModel, QuestionModel) \
            .join(QuestionModel, QuestionModel.id_question == EventXQuestionModel.id_question) \
            .filter(EventXQuestionModel.id_event == id_event) \
            .all()

        response = []

        for result in result_set:

            event_question = result[0]
            question_model = result[1]

            old_result = ResultModel.query \
                .filter(ResultModel.id_event == event_question.id_event) \
                .filter(ResultModel.id_question == event_question.id_question) \
                .filter(ResultModel.id_user == id_applicant) \
                .first()

            result_response = {}

            if old_result:
                result_response['points'] = old_result.points
            else:
                result_response['points'] = 0.

            result_response['question'] = question_model.name
            result_response['questionPoints'] = event_question.points

            response.append(result_response)

        return response

