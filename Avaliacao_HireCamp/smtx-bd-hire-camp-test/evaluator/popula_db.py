from db import db
from app import app

from models.event_x_question import EventXQuestionModel
from models.event import EventModel
from models.user import UserModel
from models.question import QuestionModel, QuestionType
from models.revoke import  RevokedTokenModel
from models.result import  ResultModel

db.init_app(app)
app.app_context().push()

db.drop_all()
db.create_all()

event = EventModel(id_event=1, name='One Day Hiring')
db.session.add(event)
db.session.commit()

question = QuestionModel(id_question=1, name='Tratamento de separadores Extras', question_type=QuestionType.other, question_class='TreatExtraSeparators')
event_x_question = EventXQuestionModel(id_event=1, id_question=1, points=5.)
db.session.add(question)
db.session.commit()

db.session.add(event_x_question)
db.session.commit()

question = QuestionModel(id_question=2, name='Salario acima da media', question_type=QuestionType.sql, question_class='AboveAvgSalary')
event_x_question = EventXQuestionModel(id_event=1, id_question=2, points=5.)
db.session.add(question)
db.session.commit()

db.session.add(event_x_question)
db.session.commit()

question = QuestionModel(id_question=3, name='Delimitadores balanceados', question_type=QuestionType.other, question_class='BalancedDelimiters')
event_x_question = EventXQuestionModel(id_event=1, id_question=3, points=10.)
db.session.add(question)
db.session.commit()

db.session.add(event_x_question)
db.session.commit()


question = QuestionModel(id_question=4, name='Lançamentos agregados por mês', question_type=QuestionType.sql, question_class='StatementPivot')
event_x_question = EventXQuestionModel(id_event=1, id_question=4, points=10.)
db.session.add(question)
db.session.commit()

db.session.add(event_x_question)
db.session.commit()


question = QuestionModel(id_question=5, name='Movie lens', question_type=QuestionType.sql, question_class='IngestionFlow')
event_x_question = EventXQuestionModel(id_event=1, id_question=5, points=24.)
db.session.add(question)
db.session.commit()

db.session.add(event_x_question)
db.session.commit()
