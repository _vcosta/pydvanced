import app
from pyspark.sql.dataframe import DataFrame
from corrector.SQL_question_corrector import SQLQuestionCorrector


class BasicIngestion:
    def __init__(self):
        self.ingestions = [('answers.movie_lens_tags', 'movie_lens.tags'),
                           ('answers.movie_lens_ratings', 'movie_lens.ratings'),
                           ('answers.movie_lens_movies', 'movie_lens.movies'),
                           ('answers.movie_lens_links', 'movie_lens.links')]

    def correct(self) -> (float, str):

        for ingestion in self.ingestions:
            answer_table = app.spark.sql(f"SELECT * FROM {ingestion[0]}")
            applicant_table = app.spark.sql(f"SELECT * FROM {ingestion[1]}")

            corrector = SQLQuestionCorrector(applicant_table, answer_table)
            is_correct, message = corrector.correct()

            if not is_correct:
                return 0., ""

        return 1., ""


class IngestionFlow:

    def __init__(self):
        self.reference_table = ''
        self.user_table = ''

    def load_correct_table(self) -> DataFrame:
        return app.spark.sql(f"SELECT * FROM {self.reference_table}")

    def load_user_table(self) -> DataFrame:
        return app.spark.sql(f"SELECT * FROM {self.user_table}")

    def correct_warehouse(self, user_table, reference_table):

        self.user_table = user_table
        self.reference_table = reference_table

        user_df = self.load_user_table()
        correct_df = self.load_correct_table()

        corrector = SQLQuestionCorrector(user_df, correct_df)
        is_correct, message = corrector.correct()

        return is_correct

    def correct(self) -> (float, str):

        points_percent = 0.

        basic_ingestion = BasicIngestion()
        is_correct, message = basic_ingestion.correct()

        if is_correct:
            points_percent += .21

        is_correct = self.correct_warehouse('movie_lens_warehouse.movie_score', 'answers.warehouse_movie_score')

        if is_correct:
            points_percent += .21

        is_correct = self.correct_warehouse('movie_lens_warehouse.genres_count', 'answers.warehouse_genres_count')

        if is_correct:
            points_percent += .29

        is_correct = self.correct_warehouse('movie_lens_warehouse.years_count', 'answers.warehouse_years_count')

        if is_correct:
            points_percent += .29
        print(points_percent)
        return points_percent, ""
