import app
from corrector.SQL_question_corrector import SQLQuestionCorrector
from pyspark.sql.dataframe import DataFrame


class StatementPivot:

    def __init__(self):
        self.reference_table = 'answers.pivoted_bank_statement'
        self.user_table = 'applicant.pivoted_bank_statement'

    def load_correct_table(self) -> DataFrame:
        return app.spark.sql(f"SELECT * FROM {self.reference_table}")

    def load_user_table(self) -> DataFrame:
        return app.spark.sql(f"SELECT * FROM {self.user_table}")

    def correct(self) -> (float, str):

        correct_df = self.load_correct_table()
        user_df = self.load_user_table()

        corrector = SQLQuestionCorrector(user_df, correct_df)
        is_correct, message = corrector.correct()

        points_percent = 1. if is_correct else 0.

        return points_percent, message