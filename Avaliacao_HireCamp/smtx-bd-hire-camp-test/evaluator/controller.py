import db
from services.question_services import QuestionServices
from flask import jsonify

app = db.app
spark = db.spark


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/question/correct/<int:id_question>")
def correct(id_question: int):

    total_points, message = QuestionServices.correct_question(id_question, 1, 1)

    return jsonify(
        totalPoints=total_points,
        message=message
    )
