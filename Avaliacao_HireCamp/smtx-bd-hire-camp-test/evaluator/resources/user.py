from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_refresh_token_required, jwt_required, get_raw_jwt)
from werkzeug.security import generate_password_hash, check_password_hash
from flask_restful import Resource, reqparse
from models.revoke import RevokedTokenModel
from models.user import UserModel

parser = reqparse.RequestParser()
parser.add_argument('name',
                    type=str
                    )
parser.add_argument('email',
                    type=str,
                    required=True,
                    help="This field cannot be blank."
                    )
parser.add_argument('password',
                    type=str,
                    required=True,
                    help="This field cannot be blank."
                    )


class UserRegister(Resource):

    def post(self):
        data = parser.parse_args()

        if UserModel.find_by_email(data['email']):
            return {"message": "A user with that email already exists"}, 400

        encrypt_password = generate_password_hash(data['password'], method='pbkdf2:sha256:1000000')
        user = UserModel(data['name'], data['email'], encrypt_password)
        user.save_to_db()

        return {"message": "User created successfully."}, 201


class UserLogin(Resource):

    def post(self):
        data = parser.parse_args()
        user = UserModel.find_by_email(data['email'])
        if not user:
            return {'message': f'User with that {user} doesn\'t exist'}, 401

        if user and check_password_hash(user.password, data['password']):
            access_token = create_access_token(identity=user.id, fresh=False)
            refresh_token = create_refresh_token(user.id)
            return {
                       'message': f'Logged in as {user.name}',
                       'access_token': access_token,
                       'refresh_token': refresh_token
                   }, 200
        else:
            return {"message": "Invalid Credentials!"}, 401


class UserLogoutAccess(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Access token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Refresh token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500



