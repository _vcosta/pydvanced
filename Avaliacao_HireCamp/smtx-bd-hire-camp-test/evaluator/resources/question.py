from flask_restful import Resource
from services.question_services import QuestionServices
from flask_jwt_extended import get_jwt_identity, jwt_required


class QuestionCorrection(Resource):

    @jwt_required
    def post(self, id_question):

        current_user = get_jwt_identity()

        total_points, question_points, message = QuestionServices.correct_question(id_question, 1, current_user)

        return {"points": total_points,
                "questionPoints": question_points,
                "message": message}


class QuestionList(Resource):

    @jwt_required
    def get(self):
        current_user = get_jwt_identity()
        return QuestionServices.get_questions(1, current_user)
