from flask_restful import Resource
from services.question_services import QuestionServices


class QuestionCorrection(Resource):
    def post(self, id_question):
        total_points, message = QuestionServices.correct_question(id_question, 1, 1)

        return total_points, message
