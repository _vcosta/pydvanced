from flask_jwt_extended import get_jwt_identity, create_access_token, jwt_refresh_token_required
from flask_restful import Resource


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}
