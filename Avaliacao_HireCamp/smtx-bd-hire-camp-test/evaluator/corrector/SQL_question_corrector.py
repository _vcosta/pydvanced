from corrector.question_corrector import QuestionCorrector
from pyspark.sql.dataframe import DataFrame


class SQLQuestionCorrector(QuestionCorrector):

    def __init__(self, user_df: DataFrame, correct_df: DataFrame):
        self.user_df = user_df
        self.correct_df = correct_df

    def _check_column_number(self) -> bool:
        return len(self.user_df.columns) == len(self.correct_df.columns)

    def _check_row_number(self) -> bool:
        return self.user_df.count() == self.correct_df.count()

    def _check_row_values(self) -> bool:
        return self.correct_df.subtract(self.user_df).count() == 0

    def correct(self) -> (bool, str):

        if not self._check_column_number():
            return False, "Tabela com quantidade distinta de colunas"

        if not self._check_row_number():
            return False, "Tabela com quantidade distinta de linhas"

        if not self._check_row_values():
            return False, "Tabela com dados diferentes"

        return True, ""
