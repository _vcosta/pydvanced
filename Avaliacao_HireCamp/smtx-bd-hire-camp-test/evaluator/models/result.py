from db import db
from datetime import datetime
from models.event import EventModel
from models.event_x_question import EventXQuestionModel
from models.question import QuestionModel
from models.user import UserModel


class ResultModel(db.Model):
    __tablename__ = 'result'

    id_event = db.Column(db.Integer, db.ForeignKey(EventXQuestionModel.id_event), primary_key=True, nullable=False)
    id_question = db.Column(db.Integer, db.ForeignKey(EventXQuestionModel.id_question), primary_key=True, nullable=False)
    id_user = db.Column(db.Integer, db.ForeignKey(UserModel.id), primary_key=True, nullable=False)
    points = db.Column(db.Float, nullable=False)
    submit_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
