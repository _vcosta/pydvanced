from db import db
from models.event import EventModel
from models.question import QuestionModel


class EventXQuestionModel(db.Model):
    __tablename__ = 'event_x_question'

    id_event = db.Column(db.Integer, db.ForeignKey(EventModel.id_event), primary_key=True, nullable=False)
    id_question = db.Column(db.Integer, db.ForeignKey(QuestionModel.id_question), primary_key=True, nullable=False)
    points = db.Column(db.Float, nullable=False)
