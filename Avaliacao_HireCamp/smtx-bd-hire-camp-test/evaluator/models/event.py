from db import db


class EventModel(db.Model):
    __tablename__ = 'event'

    id_event = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String(200), nullable=True)
