import enum
from db import db


class QuestionType(enum.Enum):
    json = 1
    sql = 2
    other = 3


class QuestionModel(db.Model):
    __tablename__ = 'question'

    id_question = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String(200), nullable=True)
    question_type = db.Column(db.Enum(QuestionType), nullable=False)
    question_class = db.Column(db.String(50), nullable=False)
