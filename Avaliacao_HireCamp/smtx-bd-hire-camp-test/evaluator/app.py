import os
from db import db
from flask import Flask
from flask_restful import Api
from pyspark.sql import SparkSession
from flask_jwt_extended import JWTManager
from models.revoke import RevokedTokenModel
from resources.refresh import TokenRefresh
from resources.question import QuestionCorrection, QuestionList
from resources.user import UserRegister, UserLogin, UserLogoutAccess, UserLogoutRefresh

app = Flask(__name__)

# SQLAchemy configs
DB_USERNAME = os.environ.get("DB_USERNAME") if os.environ.get("DB_USERNAME") is not None else "evaluator"
DB_PASS     = os.environ.get("DB_PASS") if os.environ.get("DB_PASS") is not None else "root"
DB_HOSTNAME = os.environ.get("DB_HOSTNAME") if os.environ.get("DB_HOSTNAME") is not None else "localhost"  
HIVE_METASTORE = os.environ.get("HIVE_METASTORE") if os.environ.get("HIVE_METASTORE") is not None else "0.0.0.0:9083" 

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{}:{}@{}/evaluator'.format(DB_USERNAME,DB_PASS,DB_HOSTNAME)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True

# JWT-Extended configs
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
app.secret_key = 'teste'

api = Api(app)  
jwt = JWTManager(app)
db.init_app(app)

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blacklisted(jti)


spark = SparkSession \
    .builder \
    .appName("Evaluator") \
    .enableHiveSupport() \
    .config("hive.metastore.uris", "thrift://{}".format(HIVE_METASTORE)) \
    .config("spark.ui.enabled", "false")\
    .getOrCreate()

# Resources routes
api.add_resource(UserLogin, '/login')
api.add_resource(UserRegister, '/register')
api.add_resource(TokenRefresh, '/token/refresh')
api.add_resource(UserLogoutAccess, '/logout/access')
api.add_resource(UserLogoutRefresh, '/logout/refresh')
api.add_resource(QuestionCorrection, '/question/correct/<int:id_question>')
api.add_resource(QuestionList, '/questions')

if __name__ == '__main__':
    app.run(debug=True,  host="0.0.0.0", port=5001)
