#!/bin/bash

cd Projects/notebooks/
source venv_spark/bin/activate
cd one-day-hiring

nohup jupyter notebook > ~/jupyter.log  2>&1 &

