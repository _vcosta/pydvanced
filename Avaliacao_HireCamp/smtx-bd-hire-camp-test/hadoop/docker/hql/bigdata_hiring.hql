CREATE DATABASE IF NOT EXISTS applicant LOCATION 'hdfs://namenode:8020/user/hive/warehouse/applicant.db';
CREATE DATABASE IF NOT EXISTS answers LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db';
CREATE DATABASE IF NOT EXISTS sample LOCATION 'hdfs://namenode:8020/user/hive/warehouse/sample.db';
CREATE DATABASE IF NOT EXISTS movie_lens LOCATION 'hdfs://namenode:8020/user/hive/warehouse/movie_lens.db';
CREATE DATABASE movie_lens_warehouse LOCATION 'hdfs://namenode:8020/user/hive/warehouse/movie_lens_warehouse.db';

 CREATE EXTERNAL TABLE IF NOT EXISTS `applicant.treat_extra_separators`(
 `campo_1` string,
 `campo_2` int,
 `campo_3` int,
 `campo_4` string,
 `campo_5` int,
 `campo_6` int,
 `campo_7` string,
 `campo_8` int,
 `campo_9` int,
 `campo_10` string,
 `campo_11` int,
 `campo_12` int,
 `campo_13` string,
 `campo_14` int,
 `campo_15` int,
 `campo_16` string,
 `campo_17` int,
 `campo_18` int,
 `campo_19` string,
 `campo_20` int)
 LOCATION 'hdfs://namenode:8020/user/hive/warehouse/applicant.db/treat_extra_separators';


 CREATE EXTERNAL TABLE IF NOT EXISTS `answers.treat_extra_separators`(
 `campo_1` string,
 `campo_2` int,
 `campo_3` int,
 `campo_4` string,
 `campo_5` int,
 `campo_6` int,
 `campo_7` string,
 `campo_8` int,
 `campo_9` int,
 `campo_10` string,
 `campo_11` int,
 `campo_12` int,
 `campo_13` string,
 `campo_14` int,
 `campo_15` int,
 `campo_16` string,
 `campo_17` int,
 `campo_18` int,
 `campo_19` string,
 `campo_20` int)
 LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/treat_extra_separators' ;

  --SQL-1

  CREATE EXTERNAL TABLE IF NOT EXISTS `applicant.above_avg_salary`(
   id_funcionario int,
   id_departamento int,
   salario decimal(10,2))
   LOCATION 'hdfs://namenode:8020/user/hive/warehouse/applicant.db/above_avg_salary' ;


  CREATE EXTERNAL TABLE IF NOT EXISTS sample.above_avg_salary(
   id_funcionario int,
   id_departamento int,
   salario decimal(10,2))
   LOCATION 'hdfs://namenode:8020/user/hive/warehouse/sample.db/above_avg_salary' ;


  CREATE EXTERNAL TABLE IF NOT EXISTS answers.above_avg_salary(
   id_funcionario int,
   id_departamento int,
   salario decimal(10,2))
   LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/above_avg_salary'   ;

 --Logica 2

 CREATE EXTERNAL TABLE IF NOT EXISTS `applicant.balanced_delimiters`(
 `delimiters` string,
 `expression` string,
 `is_valid` boolean)
 LOCATION 'hdfs://namenode:8020/user/hive/warehouse/applicant.db/balanced_delimiters' ;

CREATE EXTERNAL TABLE IF NOT EXISTS `answers.balanced_delimiters`(
 `delimiters` string,
 `expression` string,
 `is_valid` boolean)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/balanced_delimiters';

--SQL-2

CREATE EXTERNAL TABLE IF NOT EXISTS `sample.bank_statement`(
 `id_lancamento` int,
 `id_cliente` int,
 `data_lancamento` date,
 `vlr_lancamento` decimal(10,2))
  LOCATION 'hdfs://namenode:8020/user/hive/warehouse/sample.db/bank_statement';

CREATE EXTERNAL TABLE IF NOT EXISTS `applicant.pivoted_bank_statement`(
 `id_cliente` int,
 `vlr_janeiro` decimal(10,2),
 `vlr_fevereiro` decimal(10,2),
 `vlr_marco` decimal(10,2),
 `vlr_abril` decimal(10,2),
 `vlr_maio` decimal(10,2),
 `vlr_junho` decimal(10,2),
 `vlr_julho` decimal(10,2),
 `vlr_agosto` decimal(10,2),
 `vlr_setembro` decimal(10,2),
 `vlr_outubro` decimal(10,2),
 `vlr_novembro` decimal(10,2),
 `vlr_dezembro` decimal(10,2))
 LOCATION 'hdfs://namenode:8020/user/hive/warehouse/applicant.db/pivoted_bank_statement';

CREATE EXTERNAL TABLE IF NOT EXISTS `answers.pivoted_bank_statement`(
 `id_cliente` int,
 `vlr_janeiro` decimal(10,2),
 `vlr_fevereiro` decimal(10,2),
 `vlr_marco` decimal(10,2),
 `vlr_abril` decimal(10,2),
 `vlr_maio` decimal(10,2),
 `vlr_junho` decimal(10,2),
 `vlr_julho` decimal(10,2),
 `vlr_agosto` decimal(10,2),
 `vlr_setembro` decimal(10,2),
 `vlr_outubro` decimal(10,2),
 `vlr_novembro` decimal(10,2),
 `vlr_dezembro` decimal(10,2))
 LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/pivoted_bank_statement';


CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens.tags
(
userId INT,
movieId INT,
tag STRING,
`timestamp` BIGINT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/movie_lens.db/tags';

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens.ratings
(
userId INT,
movieId INT,
rating FLOAT,
`timestamp` BIGINT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/movie_lens.db/ratings';

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens.movies
(
movieId INT,
title STRING,
genres STRING
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/movie_lens.db/movies';

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens.links
(
movieId INT,
imdbId INT,
tmdbId INT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/movie_lens.db/links';


CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens_warehouse.movie_score
(
movieId INT,
title STRING,
avg_rating FLOAT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/movie_lens_warehouse.db/movie_score';

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens_warehouse.genres_count
(
genre STRING,
count_movies INT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/movie_lens_warehouse.db/genres_count';

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens_warehouse.years_count
(
year INT,
count_movies INT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/movie_lens_warehouse.db/years_count';


CREATE EXTERNAL TABLE IF NOT EXISTS answers.movie_lens_tags
(
userId INT,
movieId INT,
tag STRING,
`timestamp` BIGINT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/movie_lens_tags';

CREATE EXTERNAL TABLE IF NOT EXISTS answers.movie_lens_ratings
(
userId INT,
movieId INT,
rating FLOAT,
`timestamp` BIGINT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/movie_lens_ratings';

CREATE EXTERNAL TABLE IF NOT EXISTS answers.movie_lens_movies
(
movieId INT,
title STRING,
genres STRING
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/movie_lens_movies';

CREATE EXTERNAL TABLE IF NOT EXISTS answers.movie_lens_links
(
movieId INT,
imdbId INT,
tmdbId INT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/movie_lens_links';

CREATE EXTERNAL TABLE IF NOT EXISTS answers.warehouse_movie_score
(
movieId INT,
title STRING,
avg_rating FLOAT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/warehouse_movie_score';

CREATE EXTERNAL TABLE IF NOT EXISTS answers.warehouse_genres_count
(
genre STRING,
count_movies INT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/warehouse_genres_count';

CREATE EXTERNAL TABLE IF NOT EXISTS answers.warehouse_years_count
(
year INT,
count_movies INT
)
LOCATION 'hdfs://namenode:8020/user/hive/warehouse/answers.db/warehouse_years_count';
