#!/bin/bash

my_ip=`ip route get 1|awk '{print $NF;exit}'`
echo "Namenode: http://${my_ip}:50070"
echo "Datanode: http://${my_ip}:50075"
echo "Spark-master: http://${my_ip}:8080"
echo "Spark-notebook: http://${my_ip}:9001"
echo "Hue (HDFS Filebrowser): http://${my_ip}:8088/home"
echo "Evaluator: http://${my_ip}:5001"


docker-compose -f docker-compose-hive.yml up -d namenode hive-metastore-postgresql
docker-compose -f docker-compose-hive.yml up -d datanode 
docker-compose -f docker-compose-hive.yml up -d edgenode
docker-compose -f docker-compose-hive.yml up -d hive-metastore
docker-compose -f docker-compose-hive.yml up -d hive-server
docker-compose -f docker-compose-hive.yml up -d spark-master spark-worker spark-notebook hue
docker-compose -f docker-compose-hive.yml up -d evaluator
docker-compose -f docker-compose-hive.yml up -d hive-client
