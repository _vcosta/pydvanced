#!/bin/bash

return_code=1

while [ $return_code -ge 1 ]; do 
    nc -z namenode 8020; 
    return_code=$?; 
done

printf "Namenode started, creating directories"

# Creating Hadoop Dirs
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hadoop
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse

# Applicant.db
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/applicant.db/above_avg_salary
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/applicant.db/balanced_delimiters
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/applicant.db/movie_lens_links
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/applicant.db/movie_lens_movies
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/applicant.db/movie_lens_ratings
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/applicant.db/movie_lens_tags
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/applicant.db/pivoted_bank_statement
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/applicant.db/treat_extra_separators

# Movie Lens db
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/movie_lens.db/links
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/movie_lens.db/movies
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/movie_lens.db/atings
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/movie_lens.db/ags

hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/movie_lens_warehouse.db/genres_count
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/movie_lens_warehouse.db/movie_score
hdfs dfs -mkdir -p hdfs://namenode:8020/user/hive/warehouse/movie_lens_warehouse.db/years_count


# Moving directories
hdfs dfs -put -f /recursos/warehouse hdfs://namenode:8020/user/hive/
hdfs dfs -put -f /recursos/resources hdfs://namenode:8020/user/hadoop
