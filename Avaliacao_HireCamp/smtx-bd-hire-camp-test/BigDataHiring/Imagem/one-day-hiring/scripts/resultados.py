import http.client
import json
import pandas as pd


def get_results(email, password):

    conn = http.client.HTTPConnection("evaluator:5001")
    payload = {"email": email, "password": password}
    headers = {'Content-Type': "application/json"}

    conn.request("POST", "login", json.dumps(payload), headers)

    res = conn.getresponse()

    if res.code == 401:
        return "Usuário não autorizado"

    data = res.read()

    login_data = json.loads(data.decode("utf-8"))
    access_token = login_data['access_token']

    headers = {'Authorization': f"Bearer {access_token}"}

    conn.request("GET", "questions", headers=headers)

    res = conn.getresponse()
    data = res.read()
    correction_data = json.loads(data.decode("utf-8"))

    result_set = pd.DataFrame.from_dict(correction_data)
    return result_set.rename(columns={'points': 'pontos',
                               'question': 'questão',
                               'questionPoints': 'pontos da questão'})[['questão','pontos', 'pontos da questão']]
