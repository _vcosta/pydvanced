# coding=utf8
import http.client
import json


def correct_question(email, password, question_id):
    conn = http.client.HTTPConnection("evaluator:5001")
    payload = {"email": email, "password": password}
    headers = {'Content-Type': "application/json"}

    conn.request("POST", "login", json.dumps(payload), headers)

    res = conn.getresponse()
    data = res.read()

    if res.code == 401:
        return "Usuário não autorizado"

    login_data = json.loads(data.decode("utf-8"))
    access_token = login_data['access_token']

    headers = {'Authorization': f"Bearer {access_token}"}

    conn.request("POST", f"question/correct/{question_id}", headers=headers)

    res = conn.getresponse()
    data = res.read()
    correction_data = json.loads(data.decode("utf-8"))

    points = correction_data['points']
    question_points = correction_data['questionPoints']

    if points == 0.:
        resultado = "Desafio incorreto, tente novamente!"
    elif question_points == points:
        resultado = f"Deafio correto, você ganhou {str(points)} pontos"
    else:
        resultado = f"Deafio parcialmente correto, você ganhou {str(points)} pontos. Tente novamente!"

    return resultado
