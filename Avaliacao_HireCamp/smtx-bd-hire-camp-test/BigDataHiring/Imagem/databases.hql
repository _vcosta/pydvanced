CREATE DATABASE IF NOT EXISTS applicant;
CREATE DATABASE IF NOT EXISTS answers;
CREATE DATABASE IF NOT EXISTS sample;
CREATE DATABASE IF NOT EXISTS movie_lens;

 CREATE EXTERNAL TABLE IF NOT EXISTS `applicant.treat_extra_separators`(
 `campo_1` string,
 `campo_2` int,
 `campo_3` int,
 `campo_4` string,
 `campo_5` int,
 `campo_6` int,
 `campo_7` string,
 `campo_8` int,
 `campo_9` int,
 `campo_10` string,
 `campo_11` int,
 `campo_12` int,
 `campo_13` string,
 `campo_14` int,
 `campo_15` int,
 `campo_16` string,
 `campo_17` int,
 `campo_18` int,
 `campo_19` string,
 `campo_20` int);


 CREATE EXTERNAL TABLE IF NOT EXISTS `answers.treat_extra_separators`(
 `campo_1` string,
 `campo_2` int,
 `campo_3` int,
 `campo_4` string,
 `campo_5` int,
 `campo_6` int,
 `campo_7` string,
 `campo_8` int,
 `campo_9` int,
 `campo_10` string,
 `campo_11` int,
 `campo_12` int,
 `campo_13` string,
 `campo_14` int,
 `campo_15` int,
 `campo_16` string,
 `campo_17` int,
 `campo_18` int,
 `campo_19` string,
 `campo_20` int);

  --SQL-1

  CREATE EXTERNAL TABLE IF NOT EXISTS applicant.above_avg_salary(
   id_funcionario int,
   id_departamento int,
   salario decimal(10,2));

  CREATE EXTERNAL TABLE IF NOT EXISTS sample.above_avg_salary(
   id_funcionario int,
   id_departamento int,
   salario decimal(10,2));

  CREATE EXTERNAL TABLE IF NOT EXISTS answers.above_avg_salary(
   id_funcionario int,
   id_departamento int,
   salario decimal(10,2));

 --Logica 2

 CREATE EXTERNAL TABLE IF NOT EXISTS `applicant.balanced_delimiters`(
 `delimiters` string,
 `expression` string,
 `is_valid` boolean);

CREATE EXTERNAL TABLE IF NOT EXISTS `answers.balanced_delimiters`(
 `delimiters` string,
 `expression` string,
 `is_valid` boolean);

--SQL-2

CREATE EXTERNAL TABLE IF NOT EXISTS `sample.bank_statement`(
 `id_lancamento` int,
 `id_cliente` int,
 `data_lancamento` date,
 `vlr_lancamento` decimal(10,2));

CREATE EXTERNAL TABLE IF NOT EXISTS `applicant.pivoted_bank_statement`(
 `id_cliente` int,
 `vlr_janeiro` decimal(10,2),
 `vlr_fevereiro` decimal(10,2),
 `vlr_marco` decimal(10,2),
 `vlr_abril` decimal(10,2),
 `vlr_maio` decimal(10,2),
 `vlr_junho` decimal(10,2),
 `vlr_julho` decimal(10,2),
 `vlr_agosto` decimal(10,2),
 `vlr_setembro` decimal(10,2),
 `vlr_outubro` decimal(10,2),
 `vlr_novembro` decimal(10,2),
 `vlr_dezembro` decimal(10,2));

CREATE EXTERNAL TABLE IF NOT EXISTS `answers.pivoted_bank_statement`(
 `id_cliente` int,
 `vlr_janeiro` decimal(10,2),
 `vlr_fevereiro` decimal(10,2),
 `vlr_marco` decimal(10,2),
 `vlr_abril` decimal(10,2),
 `vlr_maio` decimal(10,2),
 `vlr_junho` decimal(10,2),
 `vlr_julho` decimal(10,2),
 `vlr_agosto` decimal(10,2),
 `vlr_setembro` decimal(10,2),
 `vlr_outubro` decimal(10,2),
 `vlr_novembro` decimal(10,2),
 `vlr_dezembro` decimal(10,2));


CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens.tags
(
userId INT,
movieId INT,
tag STRING,
`timestamp` BIGINT
);

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens.ratings
(
userId INT,
movieId INT,
rating FLOAT,
`timestamp` BIGINT
);

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens.movies
(
movieId INT,
title STRING,
genres STRING
);

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens.links
(
movieId INT,
imdbId INT,
tmdbId INT
);


CREATE DATABASE movie_lens_warehouse;

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens_warehouse.movie_score
(
movieId INT,
title STRING,
avg_rating FLOAT
);

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens_warehouse.genres_count
(
genre STRING,
count_movies INT
);

CREATE EXTERNAL TABLE IF NOT EXISTS movie_lens_warehouse.years_count
(
year INT,
count_movies INT
);


CREATE EXTERNAL TABLE IF NOT EXISTS answers.movie_lens_tags
(
userId INT,
movieId INT,
tag STRING,
`timestamp` BIGINT
);

CREATE EXTERNAL TABLE IF NOT EXISTS answers.movie_lens_ratings
(
userId INT,
movieId INT,
rating FLOAT,
`timestamp` BIGINT
);

CREATE EXTERNAL TABLE IF NOT EXISTS answers.movie_lens_movies
(
movieId INT,
title STRING,
genres STRING
);

CREATE EXTERNAL TABLE IF NOT EXISTS answers.movie_lens_links
(
movieId INT,
imdbId INT,
tmdbId INT
);

CREATE EXTERNAL TABLE IF NOT EXISTS answers.warehouse_movie_score
(
movieId INT,
title STRING,
avg_rating FLOAT
);

CREATE EXTERNAL TABLE IF NOT EXISTS answers.warehouse_genres_count
(
genre STRING,
count_movies INT
);

CREATE EXTERNAL TABLE IF NOT EXISTS answers.warehouse_years_count
(
year INT,
count_movies INT
);
