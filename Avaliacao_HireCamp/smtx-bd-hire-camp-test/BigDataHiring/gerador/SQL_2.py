from pyspark.sql import SparkSession
from datetime import date, timedelta
import random
import calendar

spark = SparkSession\
    .builder\
    .appName("SimpleApp")\
    .enableHiveSupport()\
    .config("hive.metastore.uris", "thrift://192.168.56.101:9083")\
    .getOrCreate()


def random_date():
    first_jan = date.today().replace(day=1, month=1)
    random_day = first_jan + timedelta(days=random.randint(0, 365 if calendar.isleap(first_jan.year) else 364))
    return random_day


lancamento = []

for id_lancamento in range(1, 50):

    id_cliente = random.randint(1, 10)
    vlr_lancamento = round(random.uniform(-1000, 1000), 2)
    data_lancamento = random_date()

    lancamento.append([id_lancamento, id_cliente, data_lancamento, vlr_lancamento])

df = spark\
    .createDataFrame(lancamento, ['id_lancamento', 'id_cliente', 'data_lancamento', 'vlr_lancamento'])\
    .coalesce(1)\
    .write \
    .insertInto("sample.bank_statement", overwrite=True)
