from pyspark.sql import SparkSession
import random


spark = SparkSession\
    .builder\
    .appName("SimpleApp")\
    .enableHiveSupport()\
    .config("hive.metastore.uris", "thrift://hive-metastore:9083")\
    .getOrCreate()

candidato = []

for id_funcionario in range(1, 1001):

    id_departamento = random.randint(1, 20)
    salario = round(random.uniform(1000, 10000), 2)

    candidato.append([id_funcionario, id_departamento, salario])

df = spark\
    .createDataFrame(candidato, ['id_funcionario', 'id_departamento', 'salario'])\
    .coalesce(1)\
    .write\
    .insertInto("sample.above_avg_salary", overwrite=True)
