import random
import string
import csv


def add_separator(original_str, quantity):

    if quantity == 0:
        return original_str

    posic = random.randint(0, len(original_str) - 1)

    new_str = original_str[:posic] + "|" + original_str[posic + 1:]

    return add_separator(new_str, quantity - 1)


sample_file = []

for line in range(0, 1000):

    row = []

    for seq, field in enumerate(range(0, 20)):

        if seq % 3 == 0:
            rand = random.random()
            rand_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))

            if rand > .9:
                new_string = add_separator(rand_string, 2)
            elif rand > .7:
                new_string = add_separator(rand_string, 1)
            elif rand > .5:
                new_string = None
            else:
                new_string = rand_string

            row.append(new_string)

        else:
            if rand > .9:
                row.append(None)
            else:
                row.append(random.randint(0, 500))

    sample_file.append(row)


output = open('dados.csv', 'w')
wr = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC, delimiter='|')
[wr.writerow(line) for line in sample_file]

output.close()

# CREATE TABLE answers.treat_extra_separators ( campo_1 STRING, campo_2 INT, campo_3 INT, campo_4 STRING, campo_5 INT, campo_6 INT, campo_7 STRING, campo_8 INT, campo_9 INT, campo_10 STRING, campo_11 INT, campo_12 INT, campo_13 STRING, campo_14 INT, campo_15 INT, campo_16 STRING, campo_17 INT, campo_18 INT, campo_19 STRING, campo_20 INT )