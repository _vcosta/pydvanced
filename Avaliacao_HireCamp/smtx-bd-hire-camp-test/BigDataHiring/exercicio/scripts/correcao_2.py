# -*- coding: utf-8 -*-
import requests
import json


def correct_question(email, password, question_id):

    base_url = "http://evaluator:5001"

    payload = {"email": email, "password": password}
    headers = {'Content-Type': "application/json"}

    response = requests.request("POST", base_url + "/login", data=json.dumps(payload), headers=headers)

    if response.status_code == 401:
        return "Usuário não autorizado"

    login_data = response.json()
    access_token = login_data['access_token']

    headers = {'Authorization': "Bearer " + access_token}

    response = requests.request("POST", base_url + "/question/correct/" + str(question_id), headers=headers)

    correction_data = response.json()

    points = correction_data['points']
    question_points = correction_data['questionPoints']

    if points == 0.:
        resultado = "Desafio incorreto, tente novamente!"
    elif question_points == points:
        resultado = "Deafio correto, você ganhou " + str(points) + " pontos"
    else:
        resultado = "Deafio parcialmente correto, você ganhou " + str(points) + " pontos. Tente novamente!"

    return resultado
