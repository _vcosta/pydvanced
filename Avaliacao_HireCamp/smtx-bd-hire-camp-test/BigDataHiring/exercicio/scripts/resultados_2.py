# -*- coding: utf-8 -*-
import requests
import json
import pandas as pd


def get_results(email, password):

    base_url = "http://evaluator:5001"

    payload = {"email": email, "password": password}
    headers = {'Content-Type': "application/json"}

    response = requests.request("POST", base_url + "/login", data=json.dumps(payload), headers=headers)

    if response.status_code == 401:
        return "Usuário não autorizado"

    login_data = response.json()
    access_token = login_data['access_token']

    headers = {'Authorization': "Bearer " + access_token}

    response = requests.request("GET", base_url + "/questions", headers=headers)

    correction_data = response.json()

    result_set = pd.DataFrame.from_dict(correction_data)
    return result_set.rename(columns={'points': 'pontos',
                               'question': 'questão',
                               'questionPoints': 'pontos da questão'})[['questão','pontos', 'pontos da questão']]
