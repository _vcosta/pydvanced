#!/bin/bash

cd Projects/evaluator/
source venv/bin/activate
nohup gunicorn -w 1 -b 127.0.0.1:4000 app:app --access-logfile ~/guinicorn_access.log --error-logfile ~/guinicorn_error.log &
