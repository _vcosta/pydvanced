#Start Flask App and Notebooks
nohup gunicorn -w 1 -b 0.0.0.0:5001 app:app --access-logfile ~/guinicorn_access.log --error-logfile ~/guinicorn_error.log &

cd /notebooks
jupyter notebook --ip='*' --port=8888 --no-browser --allow-root > /evaluator/jupyter.log  &

echo "Jupyter Token"

tail -f /evaluator/jupyter.log
