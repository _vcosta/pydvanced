echo "Start Download..."
sudo docker pull bde2020/hadoop-namenode:1.1.0-hadoop2.8-java8
sudo docker pull bde2020/hadoop-datanode:1.1.0-hadoop2.8-java8
sudo docker pull bde2020/hdfs-filebrowser:3.11
sudo docker pull bde2020/hive:2.3.2
sudo docker pull bde2020/hive-metastore-postgresql:2.3.0
sudo docker pull bde2020/spark-master:2.3.2-hadoop2.8
sudo docker pull bde2020/spark-worker:2.3.2-hadoop2.8
sudo docker pull bde2020/spark-notebook:2.1.0-hadoop2.8-hive
echo  "Complete Download \o/"
