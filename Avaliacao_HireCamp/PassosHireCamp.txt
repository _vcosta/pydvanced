A) Baixar o arquivo "smtx-bd-hire-camp-test.tar.gz" e copiar/mover para a pasta $HOME de vocês:
cp smtx-bd-hire-camp-test.tar.gz $HOME/

B) Dentro do $HOME (cd $HOME):
tar -xvzf smtx-bd-hire-camp-test.tar.gz
mv smtx-bd-hire-camp-test smtx-bd-hire-camp
cd smtx-bd-hire-camp

C) Baixar o arquivo "README.MD" e seguir os passos descritos no arquivo (pulando a seção inicial "# Extract File" já executada no passo B deste passo a passo). Os passos desse arquivo são para "startar" o engine de avaliação, a forma de acessar o Jupyter Notebook com as questões e depois, como finalizar todos os processos e containers.
