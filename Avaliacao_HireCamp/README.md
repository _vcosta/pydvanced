# Extract File
tar -xvzf smtx-bd-hire-camp-test.tar.gz
mv smtx-bd-hire-camp-test smtx-bd-hire-camp
cd smtx-bd-hire-camp

# Build Application (Flask and Notebooks) and edgenodes images
sudo su
Obs.: A ideia e que vire "root" para os comandos abaixo (ate o "curl")
docker build -f docker/Dockerfile -t semantix/evaluator .
docker build -f docker/Dockerfile_datanode -t semantix/datanode .
docker build -f docker/Dockerfile_hive_client -t semantix/hive-client .

# Start Application
cd hadoop/docker/
sh start-hadoop-spark-workbench-with-Hive.sh
Obs.: Aguardar uns 5 minutos para estabilização dos containers com as aplicações.

# Create user for test 
curl -X POST http://0.0.0.0:5001/register -d "email=nome@semantix.com.br&password=senha&name='Nome Completo'"
exit 
Obs.: Se estava usando conta "root", o exit eh para sair do "root" e voltar para o seu usuario

# Getting Notebook tokens
docker ps
docker logs container_id_ semantix/evaluator
....http://localhost:8888/?token=number_token...
_________________________________________________________
_________________________CUIDADO_________________________
# Kill/Stop all process
docker kill `echo $(docker ps -a -q)`

# Delete container
docker rm -f id_container
OU 
# Delete all containers
docker rm `echo $(docker ps -a -q)`

# Clean all
docker system prune --all
