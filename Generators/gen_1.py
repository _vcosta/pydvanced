def gen(number):
    for i in range(number):
        yield i

# lista = [i for i in range(10)]

# g = gen(10)

# for n in g:
#     print(n)

# print(g)

# for n in g:
#     print(n)

file = ["asd,123","dfg,234"]

lines = (line for line in file)
lists = (l.split(',') for l in lines)

print(type(lines), type(lists))

print(next(lists), next(lines))
print(lines, lists)