import json

# lemos o JSON em disco
conteudo = open('campeonato.json').read()
# OU lendo de uma string:
# conteudo = '[{"id": 1,"nome":"Gremio","posicao":30,"descricao":"Equipe de Porto Alegre"},{"id": 2,"nome":"Real Madrid","posicao":30,"descricao":"Equipe de Madri"}]'

lista_equipes = json.loads(conteudo)

# montamos o for para listar as equipes
for item in lista_equipes:
	print(item)

