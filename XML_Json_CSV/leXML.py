from xml.etree import ElementTree as et
# lemos o XML em disco
conteudo = et.parse("campeonato.xml")

# usamos .findall para retornar a lista de equipes
lista_equipes = conteudo.findall("equipe")

# contamos quantos registros temos em nosso arquivo
print("Equipes: ",len(lista_equipes))

# montamos o for para listar as equipes
for item in lista_equipes:
	print("Nome: ",item.find("nome").text)
	print("Posicao: ",item.find("posicao").text)
	print("Descricao: ",item.find("descricao").text)
