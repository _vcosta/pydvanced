class A:
    def process(self):
        print('A process()')
class B:
    pass
class C(A, B):
    pass
print(C.mro())   # print MRO for class C

