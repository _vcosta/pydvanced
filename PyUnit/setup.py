# -*- coding: utf-8 -*-
from setuptools import setup
setup(
    name='PyUnit',
    packages=['src', 'test'],
    test_suite='test',
)
